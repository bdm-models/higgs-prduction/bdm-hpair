# HPAIR-SCALARS

*HPAIR-SCALARS* is an extension to the original [HPAIR](http://tiger.web.psi.ch/hpair) by [Michael Spira](http://www.psi.ch/ltp-theory/michael-spira) which calculates the total cross section of Higgs boson pairs at hadron colliders. This modification includes the effects of Dark Colored Scalars in an arbitrary $SU(2)_L$ representation at LO.

# Installation
## Dependencies
`gfortran` is used to compile HPAIR.
Some more dependencies for LHAPDF might be required such as `gcc`. 

## Download
Either clone this repository or manually download it to a folder of your choice.
I will assume you have named the folder **hpair-scalars**.

## Quick installation
Open the terminal in the **hpair-scalars** folder. Run the following commands (I suggest one at a time to catch any errors):
```bash
cd scripts/INSTALL_LHAPDF
sudo ./INSTALL_LHAPDF /opt/LHAPDF 6.5.1
sudo ./INSTALL_PDF_SET /opt/LHAPDF NNPDF40_lo_as_01180
cd ../..
make -e installLHAPDFfolder=/opt/LHAPDF
```
Feel free to change the *LHAPDF* installation folder to any other you wish (ex: **/opt/LHAPDF**&rarr;**/home/user/LHAPDF**), the version (ex: **6.5.1**&rarr;**6.5.4**) or the pdf set (ex: **NNPDF40_lo_as_01180**&rarr;**NNPDF23_lo_as_0130_qed**).
Any additional pdf set you might need can be installed by simply running the corresponding line again in the folder where the script is. Ex:
```bash
cd scripts/INSTALL_LHAPDF
sudo ./INSTALL_PDF_SET /opt/LHAPDF NNPDF23_lo_as_0130_qed
```
The list of sets can be found at [LHAPDF dataset](https://lhapdf.hepforge.org/pdfsets.html).

Test by:
```bash
./run
```
which should not give an error and produce an **hpair.out** file. If it does not work check the more detailed instructions bellow.

## Step-by-Step Installation
### LHAPDF
*LHAPDF* is required. A script has been included that should automatically install it, **INSTALL_LHAPDF** in the **hpair-scalars/INSTALL_LHAPDF** folder. Run it to install:
```bash
sudo ./INSTALL_LHAPDF
```
This will install *LHAPDF* to the default folder of  **/opt/LHAPDF**  and version **6.5.1**. You can change these by, for example, running instead:
```bash
sudo ./INSTALL_LHAPDF /home/user/LHAPDF 6.5.4
```
Note that after installation the downloaded source *LHAPDF* will remain in the scripts folder for prosperity. However you can safely delete it if you so wish. I have left the version used for testing (6.5.1) in the backup folder.

More information and help on LHAPDF installation can be found on the official  page: [LHAPDF](https://lhapdf.hepforge.org/)

### LHAPDF sets
*LHAPDF* does not include any set after a fresh installation. These are found at the official site [LHAPDF dataset](https://lhapdf.hepforge.org/pdfsets.html) . You can install a set by downloading it and placing it manually into the **opt/LHAPDF/share/LHAPDF** folder (or where you have chosen to install *LHAPDF* in the previous step).
Alternatively, I have also included a script **INSTALL_PDF_SET** in the **hpair-scalars/INSTALL_LHAPDF** folder that does these two steps automatically.

Start by opening a terminal in the scripts folder. Then run the script:
```bash
sudo ./INSTALL_PDF_SET
```
This will download and copy the pdf set **NNPDF40_lo_as_01180** assuming the *LHAPDF* default installation folder of  **/opt/LHAPDF**. If in the previous step you installed *LHAPDF* in a different folder or want a different pdf set, you must indicate this. For example:
```bash
sudo ./INSTALL_LHAPDF /home/user/LHAPDF NNPDF23_lo_as_0130_qed
```
This command can be repeated to install more pdf sets.

Note that after installation a copy of the downloaded pdf set will remain in the scripts folder for prosperity. However you can safely delete it if you so wish. I have left the *NNPDF40_lo_as_01180* set used during testing in the backup folder.

More information and help on LHAPDF sets installation can be found on the official  page: [LHAPDF](https://lhapdf.hepforge.org/)

### Compiling hpair/hpair-scalars
`make` is used to compile *HPAIR*. The **makefile** can be changed for your needs (compiler, installation directories, flags for debugging, etc...) but most likely there will be no need.

Open a terminal in the **hpair-scalars** folder. Then compile with `make`:
```bash
make
```
This will assume you have installed *LHAPDF* in the **/opt/LHAPDF** folder. If you have used a different folder (ex: **/home/user/LHAPD**) then run instead:
```bash
make -e installLHAPDFfolder=/opt/LHAPDF
```
Or you can also change the variable *installLHAPDFfolder* in the **makefile** if you plan on running `make` a lot of times.

Test by:
```bash
./run
```
which should not give an error and produce an non-empty **hpair.out** file.

# Usage
The **<span>hpair.in<span>** file is used to select the parameters. A description of some of the BDM related options can be found in [!!!!!](arxiv). After running hpair with `./run` the outputs values will be found in **hpair.out**.

A bash script used to sweep the couplings and masses is included in the scripts folder, **run_sweep_parameters**. Its usage is documented through comments in the file itself. Any help needed contact ([danielstnea@gmail.com](danielstnea@gmail.com)).

# Support
If you are having any issues with installation or need help with usage please send an e-mail to danielstnea@gmail.com. I also appreciate that any bugs found (whether you have managed to fix them or not) are reported.

# Authors and Acknowledgements
The authors of this code are:
- Pedro Gabriel ([pedrogabriel347@hotmail.com](pedrogabriel347@hotmail.com))
- [Margarete Mühlleitner](https://www.itp.kit.edu/~maggie/) ([margarete.muehlleitner@kit.edu](margarete.muehlleitner@kit.edu))
- Daniel Neacsu ([danielstnea@gmail.com](danielstnea@gmail.com))
- [Rui Santos](https://cftc.ciencias.ulisboa.pt/membro.php?username=rui) ([rasantos@fc.ul.pt](rasantos@fc.ul.pt))

This modification relies on the work of [Michael Spira](http://www.psi.ch/ltp-theory/michael-spira) with [HPAIR](http://tiger.web.psi.ch/hpair).

## References
If you use this code in your work please include the following citations.

Related to the original code of *HPAIR*:
1. Pair Production of Neutral Higgs Particles in Gluon--Gluon Collisions ([arXiv:hep-ph/9603205](https://arxiv.org/abs/hep-ph/9603205))
2. Neutral Higgs-Boson Pair Production at Hadron Colliders: QCD Corrections ([arXiv:hep-ph/9805244](https://arxiv.org/abs/hep-ph/9805244))
3. NLO QCD Corrections to Higgs Pair Production including Dimension-6 Operators ([arXiv:1504.06577](https://arxiv.org/abs/1504.06577))

Related to the *BDM* modification:
1. Dark Coloured Scalars Impact on Single and Di-Higgs Production at the LHC (!!!)


Here are the *bibtex* entries to save you some time:
```tex
@article{Plehn:1996wb,
    author = "Plehn, T. and Spira, M. and Zerwas, P. M.",
    title = "{Pair production of neutral Higgs particles in gluon-gluon collisions}",
    eprint = "hep-ph/9603205",
    archivePrefix = "arXiv",
    reportNumber = "DESY-95-215",
    doi = "10.1016/0550-3213(96)00418-X",
    journal = "Nucl. Phys. B",
    volume = "479",
    pages = "46--64",
    year = "1996",
    note = "[Erratum: Nucl.Phys.B 531, 655--655 (1998)]"
}
@article{Dawson:1998py,
    author = "Dawson, S. and Dittmaier, S. and Spira, M.",
    title = "{Neutral Higgs boson pair production at hadron colliders: QCD corrections}",
    eprint = "hep-ph/9805244",
    archivePrefix = "arXiv",
    reportNumber = "DESY-98-028, BNL-HET-98-15, CERN-TH-98-135",
    doi = "10.1103/PhysRevD.58.115012",
    journal = "Phys. Rev. D",
    volume = "58",
    pages = "115012",
    year = "1998"
}
@article{Grober:2015cwa,
    author = "Grober, Ramona and Muhlleitner, Margarete and Spira, Michael and Streicher, Juraj",
    title = "{NLO QCD Corrections to Higgs Pair Production including Dimension-6 Operators}",
    eprint = "1504.06577",
    archivePrefix = "arXiv",
    primaryClass = "hep-ph",
    reportNumber = "KA-TP-08-2015, PSI-PR-15-03, RM3-TH-15-5",
    doi = "10.1007/JHEP09(2015)092",
    journal = "JHEP",
    volume = "09",
    pages = "092",
    year = "2015"
}

@article{gabriel2023dark,
      title={Dark Coloured Scalars Impact on Single and Di-Higgs Production at the LHC}, 
      author={Pedro Gabriel and Margarete Mühlleitner and Daniel Neacsu and Rui Santos},
      year={2023},
      eprint={2308.07023},
      archivePrefix={arXiv},
      primaryClass={hep-ph}
}
```

# License
As a modification to the original code by **Michael Spira** it falls under the same license  (GNU GPL v2). We ask that if you use this code then you also follow the same guidelines and requests as set by **Spira**: [HPAIR License](http://tiger.web.psi.ch/hpair/licence.html).

[GNU General Public License (GPL) version 2](http://www.gnu.org/licenses/gpl-2.0.html)
> This program is free software; you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation; either version 2 of the License, or (at
> your option) any later version.
> 
> This program is distributed in the hope that it will be useful, but
> WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
> General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program; if not, write to the Free Software
> Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
> 02110-1301, USA.


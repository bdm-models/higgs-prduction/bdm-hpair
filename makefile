installLHAPDFfolder=/opt/LHAPDF

# Makro-Definitions:

OBJS = hpair.o Cteq61Pdf.o
OBJS0 = hpair0.o Cteq61Pdf.o

LIBS =

#LIBS = /afs/psi.ch/user/s/spira/data_nobackup/LHAPDF/lib/libLHAPDF.so

LIBS = -L${installLHAPDFfolder}/lib/ -lLHAPDF -Wl,-rpath=${installLHAPDFfolder}/lib/

#FFLAGS= -fno-emulate-complex -fno-automatic -ffixed-line-length-none -ffast-math -march=pentiumpro -malign-double -Wall -fno-silent

#FC=f77

#FFLAGS= -fno-fixed-form -fno-automatic -ffixed-line-length-none -ffast-math -march=pentiumpro -malign-double -Wall

#FFLAGS=

FC=gfortran

#FFLAGS= -pc 64 -g77libs

#FC=pgf77

# Commands:
.f.o:
	$(FC) $(FFLAGS) -c $*.f

hpair: $(OBJS)
	$(FC) $(FFLAGS) $(OBJS) $(LIBS) -o run

hpair0: $(OBJS0)
	$(FC) $(FFLAGS) $(OBJS0) $(LIBS) -o run0

clean:
	rm -f $(OBJS)
